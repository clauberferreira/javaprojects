angular.module('medicalJournal').controller(
		'searchJournalCtrl',
		function($scope, $http) {

			$scope.searchPublications = function(search) {
				delete $scope.publications;
				$http.get('/publication/search/' + search).success(
						function(data, status) {
							$scope.publications = data;
						}).error(function(data, status) {
					mj.handleException(data);
				});
			};

		});