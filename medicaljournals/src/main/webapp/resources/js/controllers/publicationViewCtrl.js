angular
		.module('medicalJournal')
		.controller(
				'publicationViewCtrl',
				function($scope, $http, publicationParam) {
					$scope.publication = publicationParam.data;

					$scope.showPDF = function showPDF(url) {
						window
								.open("/resources/view/restrict/pdfViewer.html?file=/publication/download/"
										+ url);
					};

				});