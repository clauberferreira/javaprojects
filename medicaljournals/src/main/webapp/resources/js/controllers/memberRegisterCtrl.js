angular.module('medicalJournal').controller(
		'memberRegisterCtrl',
		function($scope, $route, $http, $location, memberParam) {

			$scope.memberType = $route.current.memberType;
			$scope.create = $route.current.create;
			$scope.member = memberParam.data;

			/**
			 * Call the rest api to register a member
			 */
			$scope.registerMember = function(member) {
				if ($scope.formMember.$invalid) {
					return;
				}

				$scope.member.memberType = $scope.memberType;

				$http({
					method : 'POST',
					url : $scope.create ? '/member/create' : '/member/update',
					data : $scope.member,
					headers : {
						"Content-Type" : "application/json",
						"Accept" : "application/json"
					}
				}).success(
						function(data, status) {
							if (!mj.isException(data)) {
								$scope.formMember.$setPristine();
								$location.path('/searchJournal');
								if (!$scope.create) {
									sessionStorage.setItem('userLogged', JSON
											.stringify(data));
								}
								delete $scope.member;
							} else {
								mj.handleException(data);
							}
						}).error(function(data, status) {
					mj.handleException(data);
				});
			};

		});