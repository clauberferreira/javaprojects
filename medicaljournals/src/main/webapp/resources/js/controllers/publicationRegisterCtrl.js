angular.module('medicalJournal').controller(
		'publicationRegisterCtrl',
		function($scope, $route, $http, $location, publicationParam,
				specialitiesParam) {

			$scope.create = $route.current.create;
			$scope.publication = publicationParam.data;
			if (publicationParam.data) {
				$scope.idSpeciality = $scope.publication.speciality.id;
				$scope.publication.edition = new Date(
						publicationParam.data.edition);
			}
			$scope.specialities = specialitiesParam.data;

			/**
			 * Call the rest api to register a publication
			 */
			$scope.registerPublication = function(publication) {
				if ($scope.formPublication.$invalid) {
					return;
				}
				publication.publisher = mj.getUserlogged();

				// Set the json object matching to speciality id selected
				publication.speciality = $scope.specialities.filter(function(
						speciality) {
					if (speciality.id == $scope.idSpeciality) {
						return speciality;
					}
				});
				publication.speciality = publication.speciality[0];

				publication.file = $scope.file.name;

				$http(
						{
							method : 'POST',
							url : $scope.create ? '/publication/create'
									: '/publication/update',
							data : $scope.publication,
							headers : {
								"Content-Type" : "application/json",
								"Accept" : "application/json"
							}
						}).success(function(data, status) {
					if (!mj.isException(data)) {
						$scope.formPublication.$setPristine();
						$location.path('/searchJournal');
						delete $scope.publication;
					} else {
						mj.handleException(data);
					}
				}).error(function(data, status) {
					mj.handleException(data);
				});
			};

			/**
			 * Call the rest api to upload the file. If it was successfully,
			 * after call the register publication
			 */
			$scope.uploadFileAndRegisterPublication = function() {
				var formData = new FormData();
				formData.append('file', $scope.file);

				$http({
					method : 'POST',
					url : '/publication/upload',
					data : formData,
					transformRequest : angular.identity,
					headers : {
						"Content-Type" : undefined,
					}
				}).success(function(data, status) {
					if (!mj.isException(data)) {
						$scope.registerPublication($scope.publication);
					} else {
						mj.handleException(data);
					}
				}).error(function(data, status) {
					mj.handleException(data);
				});
			};

		});