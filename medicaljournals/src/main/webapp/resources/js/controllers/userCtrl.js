angular
		.module('medicalJournal')
		.controller(
				'userCtrl',
				function($scope, $rootScope, $http, $location) {

					/**
					 * Call the rest api to login the user into application
					 */
					$scope.loginUser = function(login) {
						if ($scope.formLogin.$invalid) {
							return;
						}

						$http(
								{
									method : 'POST',
									url : '/authenticate',
									data : 'username=' + $scope.login.username
											+ '&password='
											+ $scope.login.password,
									headers : {
										"Content-Type" : "application/x-www-form-urlencoded",
										"X-Login-Ajax-call" : 'true'
									}
								}).success(
								function(data, status) {
									if (!mj.isException(data)) {
										sessionStorage.setItem('userLogged',
												JSON.stringify(data));
										window.location.replace("/");
									} else {
										mj.handleException(data);
									}

								}).error(function(data, status) {
							mj.handleException(data);
						});
					};

					/**
					 * Get the user logged into the application
					 */
					$scope.getUserlogged = mj.getUserlogged;

					/**
					 * Check if some user is logged into the application
					 */
					$scope.isUserlogged = mj.isUserlogged;

					/**
					 * Execute the user logout and redirect to searchJournal page
					 */
					$scope.logout = function() {
						$http({
							method : 'POST',
							url : '/logout'
						}).success(function(data, status) {
							if (!mj.isException(data)) {
								mj.cleanSessionLogin();
								$location.path("/searchJournal");
							} else {
								mj.handleException(data);
							}
						}).error(function(data, status) {
							mj.handleException(data);
						});

					};

					/**
					 * Check if the user logged is a publisher
					 */
					$scope.isPublisherUser = function() {
						return mj.getUserlogged().memberType == 'PUBLISHER';
					};

				});