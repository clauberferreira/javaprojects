mj = {
	isException : function(data) {
		return (data.hasOwnProperty('statusCode') && data
				.hasOwnProperty('body'));
	},

	handleException : function(data) {
		window.alert(data.body);
	},

	getUserlogged : function() {
		return JSON.parse(sessionStorage.getItem('userLogged'));
	},

	isUserlogged : function() {
		return mj.getUserlogged() != null;
	},

	cleanSessionLogin : function() {
		sessionStorage.removeItem('userLogged');
	}

};

angular.module("medicalJournal", [ "ngRoute" ]).run(
		function($rootScope, $location) {
			$rootScope.location = $location;
		});
