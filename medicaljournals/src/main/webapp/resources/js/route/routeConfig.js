angular
		.module('medicalJournal')
		.config(
				function($routeProvider, $httpProvider) {

					$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
					$httpProvider.interceptors.push('httpRequestInterceptor');

					/**
					 * Configured route to access the search journal page
					 */
					$routeProvider
							.when(
									'/searchJournal',
									{
										templateUrl : '/resources/view/public/searchJournal.html',
										controller : 'searchJournalCtrl'
									});

					/**
					 * Configured route to access the publisher register page
					 */
					$routeProvider
							.when(
									'/publisherRegister',
									{
										templateUrl : '/resources/view/public/memberRegister.html',
										controller : 'memberRegisterCtrl',
										memberType : 'PUBLISHER',
										create : true,
										resolve : {
											memberParam : function() {
												return {};
											}
										}
									});

					/**
					 * Configured route to access the publisher register edition
					 * page
					 */
					$routeProvider
							.when(
									'/publisherDetail/:id',
									{
										templateUrl : '/resources/view/public/memberRegister.html',
										controller : 'memberRegisterCtrl',
										memberType : 'PUBLISHER',
										create : false,
										resolve : {
											memberParam : function($route,
													memberRegisterService) {
												return memberRegisterService
														.findMember($route.current.params.id);
											}
										}
									});

					/**
					 * Configured route to access the subscriber register page
					 */
					$routeProvider
							.when(
									'/subscriberRegister',
									{
										templateUrl : '/resources/view/public/memberRegister.html',
										controller : 'memberRegisterCtrl',
										memberType : 'SUBSCRIBER',
										create : true,
										resolve : {
											memberParam : function() {
												return {};
											}
										}
									});

					/**
					 * Configured route to access the subscriber register
					 * edition page
					 */
					$routeProvider
							.when(
									'/subscriberDetail/:id',
									{
										templateUrl : '/resources/view/public/memberRegister.html',
										controller : 'memberRegisterCtrl',
										memberType : 'SUBSCRIBER',
										create : false,
										resolve : {
											memberParam : function($route,
													memberRegisterService) {
												return memberRegisterService
														.findMember($route.current.params.id);
											}
										}
									});

					/**
					 * Configured route to access the publication register page
					 */
					$routeProvider
							.when(
									'/publicationRegister',
									{
										templateUrl : '/resources/view/restrict/publicationRegister.html',
										controller : 'publicationRegisterCtrl',
										create : true,
										resolve : {
											publicationParam : function() {
												return {};
											},
											specialitiesParam : function(
													specialityService) {
												return specialityService
														.findAllSpecialities();
											}
										}
									});

					/**
					 * Configured route to access the publication register
					 * edition page
					 */
					$routeProvider
							.when(
									'/publicationDetail/:id',
									{
										templateUrl : '/resources/view/restrict/publicationRegister.html',
										controller : 'publicationRegisterCtrl',
										create : false,
										resolve : {
											publicationParam : function($route,
													publicationRegisterService) {
												return publicationRegisterService
														.findPublication($route.current.params.id);
											},
											specialitiesParam : function(
													specialityService) {
												return specialityService
														.findAllSpecialities();
											}
										}
									});

					/**
					 * Configured route to access the publication view page
					 */
					$routeProvider
							.when(
									'/publicationView/:id',
									{
										templateUrl : '/resources/view/restrict/publicationView.html',
										controller : 'publicationViewCtrl',
										resolve : {
											publicationParam : function($route,
													publicationRegisterService) {
												return publicationRegisterService
														.findPublication($route.current.params.id);
											}
										}
									});

					/**
					 * Configured route to access the 404 page if the user try
					 * to access an non existent page
					 */
					$routeProvider.when('/404', {
						templateUrl : '/resources/view/public/404.html',
					});

					/**
					 * Configured route to redirect the user if it try to access
					 * a security page without authorization
					 */
					$routeProvider.when('/401', {
						templateUrl : '/resources/view/public/401.html',
					});

					/**
					 * Configured route to redirect the user who to try access
					 * any different route that is configured
					 */
					$routeProvider.otherwise({
						redirectTo : '/searchJournal'
					});
				});