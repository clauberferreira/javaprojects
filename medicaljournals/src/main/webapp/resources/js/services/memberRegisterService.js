angular.module('medicalJournal').service('memberRegisterService',
		function($http) {

			/**
			 * Get the member object through the correspondent id
			 */
			this.findMember = function(id) {
				return $http.get('/member/find/' + id);
			};

		});