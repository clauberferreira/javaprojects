angular.module('medicalJournal').service('publicationRegisterService',
		function($http) {

			/**
			 * Get the publication object through the correspondent id
			 */
			this.findPublication = function(id) {
				return $http.get('/publication/find/' + id);
			};

		});