angular.module('medicalJournal').service('specialityService', function($http) {

	/**
	 * Get all specialities registered in the application
	 */
	this.findAllSpecialities = function() {
		return $http.get('/speciality/findAll');
	};

});