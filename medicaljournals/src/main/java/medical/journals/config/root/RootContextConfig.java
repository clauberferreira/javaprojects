package medical.journals.config.root;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * The root context configuration of the application - the beans in this context will be globally visible in
 * all servlet contexts.
 * 
 * @author Clauber Ferreira
 */

@Configuration
@ComponentScan({ "medical.journals.app.services", "medical.journals.app.repository",
        "medical.journals.app.init", "medical.journals.app.security", "medical.journals.app.exception" })
public class RootContextConfig {

    /**
     * Create the TransactionManager to be applied to EntityManagerFactory
     * 
     * @param entityManagerFactory
     * @param dataSource
     * @return PlatformTransactionManager
     */

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory,
            DriverManagerDataSource dataSource) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }

}
