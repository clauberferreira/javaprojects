package medical.journals.config.root;

import java.util.HashMap;
import java.util.Map;

import medical.journals.app.init.TestDataInitializer;

import org.springframework.context.annotation.Bean;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * Configuration to create a datasource, sets hibernate on create mode and inserts some test data on the
 * database.
 * 
 * @author Clauber Ferreira
 */
public abstract class EnvironmentConfiguration {

    /**
     * Return the driver class name to connect to database
     * 
     * @return String
     */
    public abstract String getDriverClassName();

    /**
     * Return the URL to connect to database
     * 
     * @return String
     */
    public abstract String getUrl();

    /**
     * Return the username to used to connect to database
     * 
     * @return String
     */
    public abstract String getUsername();

    /**
     * Return the password to used to connect to database
     * 
     * @return String
     */
    public abstract String getPassword();

    /**
     * Return the dialect database
     * 
     * @return String
     */
    public abstract String getDialectDatabase();

    /**
     * Initialize the database with some records
     * 
     * @return TestDataInitializer
     */
    @Bean(initMethod = "init")
    public TestDataInitializer initTestData() {
        return new TestDataInitializer();
    }

    /**
     * Create the datasource to access the database
     * 
     * @return DriverManagerDataSource
     */
    @Bean(name = "datasource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(getDriverClassName());
        dataSource.setUrl(getUrl());
        dataSource.setUsername(getUsername());
        dataSource.setPassword(getPassword());
        return dataSource;
    }

    /**
     * Create the EnityManagerFactory loading all entity class configured in the package
     * "medicaljournals.app.entity".
     * 
     * @param dataSource
     * @return LocalContainerEntityManagerFactoryBean
     */
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DriverManagerDataSource dataSource) {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean =
                new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(new String[] { "medical.journals.app.entity" });
        entityManagerFactoryBean.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        Map<String, Object> jpaProperties = new HashMap<>();
        jpaProperties.put("hibernate.hbm2ddl.auto", "create-drop");
        jpaProperties.put("hibernate.show_sql", "true");
        jpaProperties.put("hibernate.format_sql", "true");
        jpaProperties.put("hibernate.use_sql_comments", "true");
        jpaProperties.put("hibernate.dialect", getDialectDatabase());
        entityManagerFactoryBean.setJpaPropertyMap(jpaProperties);

        return entityManagerFactoryBean;
    }

}
