package medical.journals.config.root;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Integration testing specific configuration - creates a in-memory datasource, sets hibernate on create mode
 * and inserts some test data on the database.
 * 
 * This allows to clone the project repository and start a running application with the command
 * 
 * mvn clean install jetty:run -Dspring.profiles.active=test
 * 
 * Access http://localhost:8080/ and login with clauber.ferreira@gmail.com / test, in order to see some test
 * data, or create a new user.
 * 
 * @author Clauber Ferreira
 */
@Configuration
@EnableTransactionManagement
@Profile("test")
public class TestConfiguration extends EnvironmentConfiguration {

    @Override
    public String getDriverClassName() {
        return "org.hsqldb.jdbcDriver";
    }

    @Override
    public String getUrl() {
        return "jdbc:hsqldb:mem:mydb";
    }

    @Override
    public String getUsername() {
        return "sa";
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public String getDialectDatabase() {
        return "org.hibernate.dialect.HSQLDialect";
    }

}
