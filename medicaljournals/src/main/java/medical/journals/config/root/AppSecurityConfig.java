package medical.journals.config.root;

import javax.sql.DataSource;

import medical.journals.app.security.AjaxAuthenticationFailureHandler;
import medical.journals.app.security.AjaxAuthenticationSuccessHandler;
import medical.journals.app.security.CustomAuthenticationProvider;
import medical.journals.app.security.RestAuthenticationEntryPoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

/**
 * The Spring Security configuration for the application.
 * 
 * @author Clauber Ferreira
 */

@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/resources/view/public/**")
                .permitAll()
                .antMatchers("/resources/css/**")
                .permitAll()
                .antMatchers("/resources/js/**")
                .permitAll()
                .antMatchers("/member/**")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .formLogin()
                .defaultSuccessUrl("/")
                .loginProcessingUrl("/authenticate")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(
                        new AjaxAuthenticationSuccessHandler(
                                new SavedRequestAwareAuthenticationSuccessHandler()))
                .failureHandler(
                        new AjaxAuthenticationFailureHandler(
                                new SavedRequestAwareAuthenticationSuccessHandler())).loginPage("/").and()
                .httpBasic().and().logout().logoutUrl("/logout").logoutSuccessUrl("/").permitAll();
    }
}
