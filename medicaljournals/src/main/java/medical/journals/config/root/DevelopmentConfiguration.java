package medical.journals.config.root;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Development specific configuration - creates a localhost mysql datasource, sets hibernate on create mode
 * and inserts some test data on the database.
 * 
 * Set -Dspring.profiles.active=development to activate this config.
 * 
 * @author Clauber Ferreira
 */
@Configuration
@EnableTransactionManagement
@Profile("development")
public class DevelopmentConfiguration extends EnvironmentConfiguration {

    @Override
    public String getDriverClassName() {
        return "com.mysql.jdbc.Driver";
    }

    @Override
    public String getUrl() {
        return "jdbc:mysql://localhost/medicaljournals";
    }

    @Override
    public String getUsername() {
        return "root";
    }

    @Override
    public String getPassword() {
        return "root";
    }

    @Override
    public String getDialectDatabase() {
        return "org.hibernate.dialect.MySQLDialect";
    }

}
