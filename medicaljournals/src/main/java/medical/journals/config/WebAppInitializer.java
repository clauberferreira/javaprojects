package medical.journals.config;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

import medical.journals.app.util.Constants;
import medical.journals.config.root.AppSecurityConfig;
import medical.journals.config.root.DevelopmentConfiguration;
import medical.journals.config.root.RootContextConfig;
import medical.journals.config.root.TestConfiguration;
import medical.journals.config.servlet.ServletContextConfig;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Replacement for most of the content of web.xml, sets up the root and the servlet context config.
 * 
 * @author Clauber Ferreira
 */

public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { RootContextConfig.class, DevelopmentConfiguration.class,
                TestConfiguration.class, AppSecurityConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { ServletContextConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig(getMultipartConfigElement());
    }

    private MultipartConfigElement getMultipartConfigElement() {
        return new MultipartConfigElement(Constants.LOCATION_UPLOAD, Constants.MAX_FILE_SIZE,
                Constants.MAX_REQUEST_SIZE, Constants.FILE_SIZE_THRESHOLD);
    }

}
