package medical.journals.app.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;

/**
 * 
 * Dto class to represent the publications.
 * 
 * @author Clauber Ferreira
 */
@Entity
public class PublicationDto implements Serializable {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    private String title;

    private String content;

    private SpecialityDto speciality;

    private Date edition;

    private String file;

    private MemberDto publisher;

    /**
     * Getter id
     * 
     * @return Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter title
     * 
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter title
     * 
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter content
     * 
     * @return String
     */
    public String getContent() {
        return content;
    }

    /**
     * Setter content
     * 
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Getter speciality
     * 
     * @return SpecialityDto
     */
    public SpecialityDto getSpeciality() {
        return speciality;
    }

    /**
     * Setter speciality
     * 
     * @param speciality
     */
    public void setSpeciality(SpecialityDto speciality) {
        this.speciality = speciality;
    }

    /**
     * Getter edition
     * 
     * @return Date
     */
    public Date getEdition() {
        return edition;
    }

    /**
     * Setter edition
     * 
     * @param edition
     */
    public void setEdition(Date edition) {
        this.edition = edition;
    }

    /**
     * Getter file
     * 
     * @return String
     */
    public String getFile() {
        return file;
    }

    /**
     * Setter String
     * 
     * @param file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * Getter publisher
     * 
     * @return Publisher
     */
    public MemberDto getPublisher() {
        return publisher;
    }

    /**
     * Setter publisher
     * 
     * @param publisher
     */
    public void setPublisher(MemberDto publisher) {
        this.publisher = publisher;
    }

}
