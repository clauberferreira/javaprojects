package medical.journals.app.dto;

import java.io.Serializable;

/**
 * 
 * Dto class to represent the login.
 * 
 * @author Clauber Ferreira
 * 
 */
public class LoginDto implements Serializable {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    private MemberDto memberDto;

    /**
     * Getter username
     * 
     * @return String
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter username
     * 
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter password
     * 
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter password
     * 
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter memberDto
     * 
     * @return MemberDto
     */
    public MemberDto getMemberDto() {
        return memberDto;
    }

    /**
     * Setter memberDto
     * 
     * @param memberDto
     */
    public void setMemberDto(MemberDto memberDto) {
        this.memberDto = memberDto;
    }

}
