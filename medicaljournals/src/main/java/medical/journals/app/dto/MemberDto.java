package medical.journals.app.dto;

import java.io.Serializable;

/**
 * 
 * Dto class to represent a publisher or a subscriber.
 * 
 * @author Clauber Ferreira
 * 
 */
public class MemberDto implements Serializable {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String email;

    private String password;

    private String confirmPassword;

    private String memberType;

    /**
     * Getter id
     * 
     * @return Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter id
     * 
     * @param id Long
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter name
     * 
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter name
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter email
     * 
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter email
     * 
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter password
     * 
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter password
     * 
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter confirmPassword
     * 
     * @return String
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Setter confirmPassword
     * 
     * @param confirmPassword
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Getter memberType
     * 
     * @return MemberType
     */
    public String getMemberType() {
        return memberType;
    }

    /**
     * Setter memberType
     * 
     * @param memberType
     */
    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

}
