package medical.journals.app.dto;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * 
 * Dto class to represent the publication's speciality.
 * 
 * @author Clauber Ferreira
 */
@Entity
public class SpecialityDto implements Serializable {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    /**
     * Getter id
     * 
     * @return Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter id
     * 
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter name
     * 
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter name
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

}
