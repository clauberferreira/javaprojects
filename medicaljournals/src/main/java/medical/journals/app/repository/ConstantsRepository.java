package medical.journals.app.repository;

/**
 * Class to define all the respository constants values.
 * 
 * @author Clauber Ferreira
 */
public final class ConstantsRepository {

    /**
     * Base repository name to be found by IoC.
     */
    public static final String BASE_REPOSIORY_NAME = "BaseRepository";

    /**
     * Member repository name to be found by IoC.
     */
    public static final String MEMBER_REPOSIORY_NAME = "MemberRepository";

    /**
     * Publication repository name to be found by IoC.
     */
    public static final String PUBLICATION_REPOSIORY_NAME = "PublicationRepository";

    private ConstantsRepository() {

    }
}
