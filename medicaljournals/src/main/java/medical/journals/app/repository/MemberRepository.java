package medical.journals.app.repository;

import medical.journals.app.entity.Member;

/**
 * 
 * Repository interface for the Member entity
 * 
 * @author Clauber Ferreira
 */
public interface MemberRepository extends BaseRepository<Member, Long> {

    /**
     * Finds a member given its email.
     * 
     * @param email The email of the searched member.
     * @return A matching member, or null if no member found.
     */
    Member findMemberByEmail(String email);
}
