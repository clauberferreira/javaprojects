package medical.journals.app.repository;

import java.util.List;

import medical.journals.app.entity.Publication;

/**
 * 
 * Repository interface for the Publication entity
 * 
 * @author Clauber Ferreira
 */
public interface PublicationRepository extends BaseRepository<Publication, Long> {

    /**
     * Search for publications that match with title, content, or speciality
     * 
     * @param search the searched word
     * @return Some matching records
     */
    List<Publication> searchPublications(String search);
}
