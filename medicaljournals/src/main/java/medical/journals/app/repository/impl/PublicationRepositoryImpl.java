package medical.journals.app.repository.impl;

import java.util.List;

import medical.journals.app.entity.Publication;
import medical.journals.app.repository.ConstantsRepository;
import medical.journals.app.repository.PublicationRepository;

import org.springframework.stereotype.Repository;

/**
 * 
 * Repository class for the Publication entity
 * 
 * @author Clauber Ferreira
 */
@Repository(ConstantsRepository.PUBLICATION_REPOSIORY_NAME)
public class PublicationRepositoryImpl extends BaseRepositoryImpl<Publication, Long> implements
        PublicationRepository {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public List<Publication> searchPublications(String search) {
        StringBuilder query = new StringBuilder();
        query.append("from Publication pu ");
        query.append("join fetch pu.speciality s ");
        query.append("join fetch pu.publisher ps ");
        query.append("where pu.title like :search ");
        query.append("or pu.content like :search ");
        query.append("or s.name like :search");

        return getEntityManager().createQuery(query.toString(), Publication.class)
                .setParameter("search", "%".concat(search).concat("%")).getResultList();
    }
}
