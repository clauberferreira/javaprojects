package medical.journals.app.repository;

import java.io.Serializable;
import java.util.List;

import medical.journals.app.entity.BaseEntity;

/**
 * Interface that define default repository operations.
 * 
 * @param <E> Entity
 * @param <I> Entity identifier
 * 
 * @author Clauber Ferreira
 */
public interface BaseRepository<E extends BaseEntity<I>, I extends Serializable> extends Serializable {

    /**
     * Persist the object.
     * 
     * @param e
     * @return E
     */
    E persist(E e);

    /**
     * Find the object matching the id of the specified class.
     * 
     * @param e Class
     * @param id Serializable
     * @return E
     */
    E find(Class<E> e, I id);

    /**
     * Update the object.
     * 
     * @param e E
     * @return E
     */
    E merge(E e);

    /**
     * Remove the object.
     * 
     * @param e E
     */
    void remove(E e);

    /**
     * Find all objects of the specified class.
     * 
     * @param e E
     * @return List<E>
     */
    List<E> findAll(Class<E> e);
}
