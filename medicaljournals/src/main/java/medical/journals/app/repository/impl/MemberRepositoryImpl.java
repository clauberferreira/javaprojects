package medical.journals.app.repository.impl;

import java.util.List;

import medical.journals.app.entity.Member;
import medical.journals.app.repository.ConstantsRepository;
import medical.journals.app.repository.MemberRepository;

import org.springframework.stereotype.Repository;

/**
 * 
 * Repository class for the Member entity
 * 
 * @author Clauber Ferreira
 */
@Repository(ConstantsRepository.MEMBER_REPOSIORY_NAME)
public class MemberRepositoryImpl extends BaseRepositoryImpl<Member, Long> implements MemberRepository {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public Member findMemberByEmail(String email) {

        List<Member> publishers =
                getEntityManager().createQuery("from Member where email = :email", Member.class)
                        .setParameter("email", email).getResultList();

        return publishers.size() == 1 ? publishers.get(0) : null;
    }

}
