package medical.journals.app.repository.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import medical.journals.app.entity.BaseEntity;
import medical.journals.app.repository.BaseRepository;
import medical.journals.app.repository.ConstantsRepository;

import org.springframework.stereotype.Repository;

/**
 * Class that define default repository operations.
 * 
 * @param <E> Entity
 * @param <I> Entity identifier
 * 
 * @author Clauber Ferreira
 */
@Repository(ConstantsRepository.BASE_REPOSIORY_NAME)
public class BaseRepositoryImpl<E extends BaseEntity<I>, I extends Serializable> implements
        BaseRepository<E, I> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @PersistenceContext
    private transient EntityManager entityManager;

    /**
     * Getter entityManager
     * 
     * @return EntityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public E persist(E e) {
        getEntityManager().persist(e);
        return e;
    }

    @Override
    public E find(Class<E> e, I id) {
        return getEntityManager().find(e, id);
    }

    @Override
    public E merge(E e) {
        return getEntityManager().merge(e);
    }

    @Override
    public void remove(E e) {
        getEntityManager().remove(e);
    }

    @Override
    public List<E> findAll(Class<E> e) {
        return getEntityManager().createQuery("from ".concat(e.getSimpleName()), e).getResultList();
    }

}
