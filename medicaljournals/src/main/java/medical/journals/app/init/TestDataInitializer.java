package medical.journals.app.init;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import medical.journals.app.entity.Member;
import medical.journals.app.entity.Publication;
import medical.journals.app.entity.Speciality;
import medical.journals.app.enums.MemberType;
import medical.journals.app.util.Constants;
import medical.journals.app.util.Cryptography;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * This is a initializing bean that inserts some test data in the database. It is only active in the
 * development and test profile.
 * 
 * @author Clauber Ferreira
 * 
 */
@Component
public class TestDataInitializer {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    /**
     * Initialize the database with some records
     */
    public void init() {

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

        /**
         * Initializing domain data records
         */
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        initializeSpeciality(session);
        initializePublisher(session);
        initializeSubscriber(session);

        transaction.commit();

        /**
         * Initializing the publication records
         */
        transaction = session.beginTransaction();

        initializePublication(session);

        transaction.commit();

    }

    /**
     * Initialize the database with some speciality records
     * 
     * @param session
     */
    private void initializeSpeciality(Session session) {
        List<String> specialityNames = new ArrayList<>();

        specialityNames.add("Cardiology");
        specialityNames.add("Allergy");
        specialityNames.add("Endocrinology");
        specialityNames.add("Gastroenterology");
        specialityNames.add("Geriatrics");
        specialityNames.add("Neurology");
        specialityNames.add("Ophthalmology");
        specialityNames.add("Paediatrics");
        specialityNames.add("Urology");
        specialityNames.add("Radiology");

        for (String specialityName : specialityNames) {
            Speciality speciality = new Speciality();
            speciality.setName(specialityName);
            session.persist(speciality);
        }
    }

    /**
     * Initialize the database with some publisher records
     * 
     * @param session
     */
    private void initializePublisher(Session session) {
        Member clauber = new Member();

        clauber.setName("Clauber Ferreira");
        clauber.setEmail("clauber.ferreira@gmail.com");
        clauber.setPassword(Cryptography.cryptography("test"));
        clauber.setMemberType(MemberType.PUBLISHER);

        session.persist(clauber);

        Member john = new Member();

        john.setName("john Smith");
        john.setEmail("john.smith@gmail.com");
        john.setPassword(Cryptography.cryptography("test2"));
        john.setMemberType(MemberType.PUBLISHER);

        session.persist(john);
    }

    /**
     * Initialize the database with some subscriber records
     * 
     * @param session
     */
    private void initializeSubscriber(Session session) {
        Member michael = new Member();
        michael.setName("Michael palmer");
        michael.setEmail("michael.palmer@yahoo.com");
        michael.setPassword(Cryptography.cryptography("test"));
        michael.setMemberType(MemberType.SUBSCRIBER);
        session.persist(michael);

        Member stepanie = new Member();
        stepanie.setName("Stefanie cherry");
        stepanie.setEmail("stefanie_cherry@yahoo.com");
        stepanie.setPassword(Cryptography.cryptography("test2"));
        stepanie.setMemberType(MemberType.SUBSCRIBER);
        session.persist(stepanie);
    }

    /**
     * Initialize the database with some publication records
     * 
     * @param session
     */
    @SuppressWarnings(Constants.UNCHECKED)
    private void initializePublication(Session session) {
        List<Speciality> specialities = session.createQuery("from Speciality").list();
        List<Member> publishers = session.createQuery("from Member where memberType = 0").list();

        Publication hypertension = new Publication();
        hypertension.setTitle("Hypertension");
        hypertension.setContent("Introduction to the American Heart Association’s "
                + "Hypertension Strategically Focused Research Network.");
        Calendar hypertensionEdition = new GregorianCalendar(2016, 3, 1);
        hypertension.setEdition(hypertensionEdition.getTime());
        hypertension.setSpeciality(specialities.get(0));
        hypertension.setPublisher(publishers.get(0));
        hypertension.setFile("Hypertension.pdf");
        session.persist(hypertension);
    }

}
