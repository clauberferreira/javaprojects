package medical.journals.app.enums;

/**
 * Enum to indentify the type of member.
 * 
 * @author Clauber Ferreira
 */
public enum MemberType {

    /**
     * identify a member as a publisher
     */
    PUBLISHER,

    /**
     * identify a member as subsbriber
     */
    SUBSCRIBER

}
