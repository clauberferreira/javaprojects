package medical.journals.app.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * Entity class to represent the publications.
 * 
 * @author Clauber Ferreira
 * 
 */
@Entity
public class Publication extends BaseEntity<Long> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    private String title;

    @NotNull
    @Size(max = 500)
    private String content;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "speciality", nullable = false)
    private Speciality speciality;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date edition;

    @NotNull
    @Size(max = 255)
    private String file;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisher", nullable = false)
    private Member publisher;

    /**
     * Getter id
     * 
     * @return Long
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Setter id
     * 
     * @param id
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter title
     * 
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter title
     * 
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter content
     * 
     * @return String
     */
    public String getContent() {
        return content;
    }

    /**
     * Setter content
     * 
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Getter speciality
     * 
     * @return Speciality
     */
    public Speciality getSpeciality() {
        return speciality;
    }

    /**
     * Setter speciality
     * 
     * @param speciality
     */
    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    /**
     * Getter edition
     * 
     * @return Date
     */
    public Date getEdition() {
        return edition;
    }

    /**
     * Setter edition
     * 
     * @param edition
     */
    public void setEdition(Date edition) {
        this.edition = edition;
    }

    /**
     * Getter file
     * 
     * @return String
     */
    public String getFile() {
        return file;
    }

    /**
     * Setter String
     * 
     * @param file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * Getter publisher
     * 
     * @return Publisher
     */
    public Member getPublisher() {
        return publisher;
    }

    /**
     * Setter publisher
     * 
     * @param publisher
     */
    public void setPublisher(Member publisher) {
        this.publisher = publisher;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Publication)) {
            return false;
        }
        Publication other = (Publication) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
