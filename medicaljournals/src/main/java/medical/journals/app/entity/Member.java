package medical.journals.app.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import medical.journals.app.enums.MemberType;

import org.hibernate.validator.constraints.Email;

/**
 * 
 * Entity class to represent a Publisher or Subscriber.
 * 
 * @author Clauber Ferreira
 * 
 */
@Entity
public class Member extends BaseEntity<Long> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 60)
    private String name;

    @NotNull
    @Email
    @Size(max = 150)
    private String email;

    @NotNull
    @Size(max = 255)
    private String password;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private MemberType memberType;

    /**
     * Getter id
     * 
     * @return Long
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Setter id
     * 
     * @param id
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter name
     * 
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter name
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter email
     * 
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter email
     * 
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter password
     * 
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter password
     * 
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter memberType
     * 
     * @return MemberType
     */
    public MemberType getMemberType() {
        return memberType;
    }

    /**
     * Setter memberType
     * 
     * @param memberType
     */
    public void setMemberType(MemberType memberType) {
        this.memberType = memberType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Member)) {
            return false;
        }
        Member other = (Member) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
