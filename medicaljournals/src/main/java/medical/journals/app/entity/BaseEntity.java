package medical.journals.app.entity;

import java.io.Serializable;

/**
 * 
 * Base entity abstract class which implements Serializable interface. All others entities in the application
 * will inherit it.
 * 
 * @author Clauber Ferreira
 * @param <I> The generic type for the base entity.
 * 
 */
public abstract class BaseEntity<I extends Serializable> implements Serializable {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Getter id
     * 
     * @return I Serializable
     */
    public abstract I getId();

    /**
     * Setter id
     * 
     * @param id Serializable
     */
    public abstract void setId(I id);
}
