package medical.journals.app.services;

import java.io.Serializable;
import java.util.List;

import medical.journals.app.entity.BaseEntity;

/**
 * Interface that define default service operations.
 * 
 * @param <E> Entity
 * @param <I> Entity identifier
 * 
 * @author Clauber Ferreira
 */
public interface BaseService<E extends BaseEntity<I>, I extends Serializable> extends Serializable {

    /**
     * Persist the base entity object.
     * 
     * @param e
     * @return E
     */
    E persist(E e);

    /**
     * Find the base entity object matching the id of the specified class.
     * 
     * @param e Base entity class
     * @param id Serializable
     * @return E
     */
    E find(Class<E> e, I id);

    /**
     * Update the base entity object.
     * 
     * @param e E
     * @return E
     */
    E merge(E e);

    /**
     * Remove the base entity object.
     * 
     * @param e E
     */
    void remove(E e);

    /**
     * Find all base entities objects of the specified class.
     * 
     * @param e Base entity class
     * @return List<E>
     */
    List<E> findAll(Class<E> e);
}
