package medical.journals.app.services.impl;

import medical.journals.app.converter.MemberConverter;
import medical.journals.app.dto.LoginDto;
import medical.journals.app.dto.MemberDto;
import medical.journals.app.entity.Member;
import medical.journals.app.services.ConstantsService;
import medical.journals.app.services.LoginService;
import medical.journals.app.services.MemberService;
import medical.journals.app.util.Cryptography;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Service class to implement the login's operation.
 * 
 * @author Clauber Ferreira
 */
@Service(ConstantsService.LOGIN_SERVICE_NAME)
@Transactional
public class LoginServiceImpl implements LoginService {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private MemberService memberService;

    private void validateUser(Member member, String password) {
        if (member == null) {
            throw new UsernameNotFoundException("User not registered.");
        }
        if (!member.getPassword().equals(Cryptography.cryptography(password))) {
            throw new BadCredentialsException("This password is incorrect.");
        }
    }

    @Override
    public LoginDto loginUser(String username, String password) {
        Member member = memberService.findMemberByEmail(username);
        validateUser(member, password);
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername(username);
        loginDto.setPassword(Cryptography.cryptography(password));
        loginDto.setMemberDto(new MemberConverter().convertEntityToDto(member, new MemberDto()));
        return loginDto;
    }
}
