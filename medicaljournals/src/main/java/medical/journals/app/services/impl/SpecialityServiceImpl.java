package medical.journals.app.services.impl;

import java.util.ArrayList;
import java.util.List;

import medical.journals.app.converter.SpecialityConverter;
import medical.journals.app.dto.SpecialityDto;
import medical.journals.app.entity.Speciality;
import medical.journals.app.services.ConstantsService;
import medical.journals.app.services.SpecialityService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Service class to implement the speciality's operation.
 * 
 * @author Clauber Ferreira
 */
@Service(ConstantsService.SPECIALITY_SERVICE_NAME)
@Transactional
public class SpecialityServiceImpl extends BaseServiceImpl<Speciality, Long> implements SpecialityService {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public List<SpecialityDto> findAllSpecialities() {
        List<Speciality> specialities = findAll(Speciality.class);
        List<SpecialityDto> specialitiesDto = new ArrayList<>();

        for (Speciality speciality : specialities) {
            specialitiesDto.add(new SpecialityConverter()
                    .convertEntityToDto(speciality, new SpecialityDto()));
        }
        return specialitiesDto;
    }

}
