package medical.journals.app.services.impl;

import medical.journals.app.converter.MemberConverter;
import medical.journals.app.dto.MemberDto;
import medical.journals.app.entity.Member;
import medical.journals.app.exception.BusinessException;
import medical.journals.app.repository.MemberRepository;
import medical.journals.app.services.ConstantsService;
import medical.journals.app.services.MemberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Service class to implement the member's operation.
 * 
 * @author Clauber Ferreira
 */
@Service(ConstantsService.MEMBER_SERVICE_NAME)
@Transactional
public class MemberServiceImpl extends BaseServiceImpl<Member, Long> implements MemberService {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private MemberRepository memberRepository;

    private MemberConverter converter = new MemberConverter();

    @Transactional(readOnly = true)
    @Override
    public Member findMemberByEmail(String email) {
        return memberRepository.findMemberByEmail(email);
    }

    private void validateMember(MemberDto memberDto) throws BusinessException {
        if (!memberDto.getPassword().equals(memberDto.getConfirmPassword())) {
            throw new BusinessException("The password confirmation is invalid.");
        }
    }

    @Override
    public void createNewMember(MemberDto memberDto) throws BusinessException {
        validateMember(memberDto);
        persist(converter.convertDtoToEntity(memberDto, new Member()));
    }

    @Override
    public MemberDto updateMember(MemberDto memberDto) throws BusinessException {
        Member member = find(Member.class, memberDto.getId());
        validateMember(memberDto);
        converter.convertDtoToEntity(memberDto, member);
        merge(member);
        return memberDto;
    }

    @Override
    public MemberDto findMember(Long id) throws BusinessException {
        MemberDto memberDto = converter.convertEntityToDto(find(Member.class, id), new MemberDto());
        if (memberDto != null) {
            memberDto.setPassword(null);
            memberDto.setConfirmPassword(null);
        }
        return memberDto;
    }

}
