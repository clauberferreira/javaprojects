package medical.journals.app.services;

import medical.journals.app.dto.MemberDto;
import medical.journals.app.entity.Member;
import medical.journals.app.exception.BusinessException;

/**
 * 
 * Service interface to work with member's operation.
 * 
 * @author Clauber Ferreira
 */
public interface MemberService extends BaseService<Member, Long> {

    /**
     * Finds a member given its email.
     * 
     * @param email The email of the searched member.
     * @return A matching member, or null if no member found.
     */
    Member findMemberByEmail(String email);

    /**
     * Create a new member if all validation rules was accepted.
     * 
     * @param memberDto
     * @throws BusinessException Thrown when some rule been invalid
     */
    void createNewMember(MemberDto memberDto) throws BusinessException;

    /**
     * Update an existing member if all validation rules was accepted.
     * 
     * @param memberDto
     * @return Updated MemberDto
     * @throws BusinessException Thrown when some rule been invalid
     */
    MemberDto updateMember(MemberDto memberDto) throws BusinessException;

    /**
     * Find an existing member
     * 
     * @param id
     * @return memberDto
     * @throws BusinessException
     */
    MemberDto findMember(Long id) throws BusinessException;
}
