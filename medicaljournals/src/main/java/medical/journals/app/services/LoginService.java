package medical.journals.app.services;

import java.io.Serializable;

import medical.journals.app.dto.LoginDto;

/**
 * 
 * Service interface to work with login's operation.
 * 
 * @author Clauber Ferreira
 */
public interface LoginService extends Serializable {

    /**
     * Validate and login the user into the application
     * 
     * @param username
     * @param password
     * @return the encountered and validated member
     */
    LoginDto loginUser(String username, String password);

}
