package medical.journals.app.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import medical.journals.app.converter.PublicationConverter;
import medical.journals.app.dto.PublicationDto;
import medical.journals.app.entity.Publication;
import medical.journals.app.exception.BusinessException;
import medical.journals.app.exception.TechnicalException;
import medical.journals.app.repository.PublicationRepository;
import medical.journals.app.services.ConstantsService;
import medical.journals.app.services.PublicationService;
import medical.journals.app.util.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * Service class to implement the publication's operation.
 * 
 * @author Clauber Ferreira
 */
@Service(ConstantsService.PUBLICATION_SERVICE_NAME)
@Transactional
public class PublicationServiceImpl extends BaseServiceImpl<Publication, Long> implements PublicationService {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private PublicationRepository publicationRepository;

    private PublicationConverter converter = new PublicationConverter();

    private void validateUpdate(MultipartFile file) throws BusinessException {
        if (!file.getOriginalFilename().toLowerCase().endsWith(Constants.PDF_FILE)) {
            throw new BusinessException("This file isn't a PDF format.");
        }

        if (file.getSize() > Constants.MAX_FILE_SIZE) {
            throw new BusinessException("This file is too big. The max permitted size is: ".concat(
                    String.valueOf(Constants.MAX_FILE_SIZE / Constants.ONE_MB_FILE_SIZE)).concat(" MB"));
        }
    }

    @Override
    public void createNewPublication(PublicationDto publicationDto) throws BusinessException {
        persist(converter.convertDtoToEntity(publicationDto, new Publication()));
    }

    @Override
    public PublicationDto updatePublication(PublicationDto publicationDto) throws BusinessException {
        Publication publication = find(Publication.class, publicationDto.getId());
        merge(converter.convertDtoToEntity(publicationDto, publication));
        return publicationDto;
    }

    @Override
    public PublicationDto findPublication(Long id) throws BusinessException {
        return converter.convertEntityToDto(find(Publication.class, id), new PublicationDto());
    }

    @Override
    public void uploadFile(MultipartFile file) throws BusinessException {
        validateUpdate(file);
        try {
            file.transferTo(new File(Constants.LOCATION_UPLOAD.concat(file.getOriginalFilename())));
        } catch (IllegalStateException | IOException e) {
            throw new TechnicalException(e.getMessage(), e);
        }
    }

    @Override
    public List<PublicationDto> searchPublications(String search) throws BusinessException {
        List<Publication> publications = publicationRepository.searchPublications(search);
        List<PublicationDto> publicationsDto = new ArrayList<>();
        for (Publication publication : publications) {
            publicationsDto.add(converter.convertEntityToDto(publication, new PublicationDto()));
        }
        return publicationsDto;
    }

}
