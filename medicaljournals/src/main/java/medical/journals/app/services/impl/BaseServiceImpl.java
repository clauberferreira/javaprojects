package medical.journals.app.services.impl;

import java.io.Serializable;
import java.util.List;

import medical.journals.app.entity.BaseEntity;
import medical.journals.app.repository.BaseRepository;
import medical.journals.app.repository.ConstantsRepository;
import medical.journals.app.services.BaseService;
import medical.journals.app.services.ConstantsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Class that define default service operations.
 * 
 * @param <E> Entity
 * @param <I> Entity identifier
 * 
 * @author Clauber Ferreira
 */
@Transactional
@Service(ConstantsService.BASE_SERVICE_NAME)
public class BaseServiceImpl<E extends BaseEntity<I>, I extends Serializable> implements BaseService<E, I> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    @Qualifier(ConstantsRepository.BASE_REPOSIORY_NAME)
    private BaseRepository<E, I> baseRepository;

    @Override
    public E persist(E e) {
        return baseRepository.persist(e);
    }

    @Override
    public E find(Class<E> e, I id) {
        return baseRepository.find(e, id);
    }

    @Override
    public E merge(E e) {
        return baseRepository.merge(e);
    }

    @Override
    public void remove(E e) {
        baseRepository.remove(e);
    }

    @Override
    public List<E> findAll(Class<E> e) {
        return baseRepository.findAll(e);
    }

}
