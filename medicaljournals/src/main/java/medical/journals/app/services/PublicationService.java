package medical.journals.app.services;

import java.util.List;

import medical.journals.app.dto.PublicationDto;
import medical.journals.app.entity.Publication;
import medical.journals.app.exception.BusinessException;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * Service interface to work with publication's operation.
 * 
 * @author Clauber Ferreira
 */
public interface PublicationService extends BaseService<Publication, Long> {

    /**
     * Create a new publication if all validation rules was accepted.
     * 
     * @param publicationDto
     * @throws BusinessException Thrown when some rule been invalid
     */
    void createNewPublication(PublicationDto publicationDto) throws BusinessException;

    /**
     * Update an existing publication if all validation rules was accepted.
     * 
     * @param publicationDto
     * @return Updated PublicationDto
     * @throws BusinessException Thrown when some rule been invalid
     */
    PublicationDto updatePublication(PublicationDto publicationDto) throws BusinessException;

    /**
     * Find an existing publication
     * 
     * @param id
     * @return PublicationDto
     * @throws BusinessException
     */
    PublicationDto findPublication(Long id) throws BusinessException;

    /**
     * Validate the rules and upload the file to configured directory.
     * 
     * @param file
     * @throws BusinessException
     */
    void uploadFile(MultipartFile file) throws BusinessException;

    /**
     * Search for publications that match with title, content, or speciality
     * 
     * @param search the searched word
     * @return Some matching records
     * @throws BusinessException
     */
    List<PublicationDto> searchPublications(String search) throws BusinessException;
}
