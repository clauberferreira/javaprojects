package medical.journals.app.services;

/**
 * Class to define all the service constants values.
 * 
 * @author Clauber Ferreira
 */
public final class ConstantsService {

    /**
     * Base service name to be found by IoC.
     */
    public static final String BASE_SERVICE_NAME = "BaseService";

    /**
     * Login service name to be found by IoC.
     */
    public static final String LOGIN_SERVICE_NAME = "LoginService";

    /**
     * Member service name to be found by IoC.
     */
    public static final String MEMBER_SERVICE_NAME = "MemberService";

    /**
     * Publication service name to be found by IoC.
     */
    public static final String PUBLICATION_SERVICE_NAME = "PublicationService";

    /**
     * Speciality service name to be found by IoC.
     */
    public static final String SPECIALITY_SERVICE_NAME = "SpecialityService";

    private ConstantsService() {

    }
}
