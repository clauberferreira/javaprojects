package medical.journals.app.services;

import java.util.List;

import medical.journals.app.dto.SpecialityDto;
import medical.journals.app.entity.Speciality;

/**
 * 
 * Service interface to work with speciality's operation.
 * 
 * @author Clauber Ferreira
 */
public interface SpecialityService extends BaseService<Speciality, Long> {

    /**
     * Find all specialities.
     * 
     * @return List<SpecialityDto>
     */
    List<SpecialityDto> findAllSpecialities();
}
