package medical.journals.app.converter;

import medical.journals.app.dto.MemberDto;
import medical.journals.app.entity.Member;
import medical.journals.app.enums.MemberType;
import medical.journals.app.util.Cryptography;

/**
 * Convert a Member entity to MemberDto and a MemberDto to Member
 * 
 * @author Clauber Ferreira
 */
public class MemberConverter implements Converter<Member, MemberDto, Long> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public Member convertDtoToEntity(MemberDto dto, Member entity) {
        if (entity == null || dto == null) {
            return null;
        }
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setMemberType(MemberType.valueOf(dto.getMemberType()));
        if (dto.getPassword() != null) {
            entity.setPassword(Cryptography.cryptography(dto.getPassword()));
        }
        dto.setPassword(entity.getPassword());
        dto.setConfirmPassword(entity.getPassword());
        return entity;
    }

    @Override
    public MemberDto convertEntityToDto(Member entity, MemberDto dto) {
        if (entity == null || dto == null) {
            return null;
        }
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setMemberType(entity.getMemberType().name());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setConfirmPassword(entity.getPassword());
        return dto;
    }

}
