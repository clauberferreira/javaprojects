package medical.journals.app.converter;

import medical.journals.app.dto.MemberDto;
import medical.journals.app.dto.PublicationDto;
import medical.journals.app.dto.SpecialityDto;
import medical.journals.app.entity.Member;
import medical.journals.app.entity.Publication;
import medical.journals.app.entity.Speciality;

/**
 * Convert a Publication entity to PublicationDto and a PublicationDto to Publication
 * 
 * @author Clauber Ferreira
 */
public class PublicationConverter implements Converter<Publication, PublicationDto, Long> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public Publication convertDtoToEntity(PublicationDto dto, Publication entity) {
        if (entity == null || dto == null) {
            return null;
        }
        entity.setId(dto.getId());
        entity.setContent(dto.getContent());
        entity.setEdition(dto.getEdition());
        entity.setPublisher(new MemberConverter().convertDtoToEntity(dto.getPublisher(), new Member()));
        entity.setTitle(dto.getTitle());
        entity.setSpeciality(new SpecialityConverter().convertDtoToEntity(dto.getSpeciality(),
                new Speciality()));
        entity.setFile(dto.getFile());
        return entity;
    }

    @Override
    public PublicationDto convertEntityToDto(Publication entity, PublicationDto dto) {
        if (entity == null || dto == null) {
            return null;
        }
        dto.setId(entity.getId());
        dto.setContent(entity.getContent());
        dto.setEdition(entity.getEdition());
        dto.setPublisher(new MemberConverter().convertEntityToDto(entity.getPublisher(), new MemberDto()));
        dto.setTitle(entity.getTitle());
        dto.setSpeciality(new SpecialityConverter().convertEntityToDto(entity.getSpeciality(),
                new SpecialityDto()));
        dto.setFile(entity.getFile());
        return dto;
    }

}
