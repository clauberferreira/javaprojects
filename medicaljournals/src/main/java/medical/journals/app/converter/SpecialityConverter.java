package medical.journals.app.converter;

import medical.journals.app.dto.SpecialityDto;
import medical.journals.app.entity.Speciality;

/**
 * Convert a Speciality entity to SpecialityDto and a SpecialityDto to Speciality
 * 
 * @author Clauber Ferreira
 */
public class SpecialityConverter implements Converter<Speciality, SpecialityDto, Long> {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public Speciality convertDtoToEntity(SpecialityDto dto, Speciality entity) {
        if (entity == null || dto == null) {
            return null;
        }
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    public SpecialityDto convertEntityToDto(Speciality entity, SpecialityDto dto) {
        if (entity == null || dto == null) {
            return null;
        }
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

}
