package medical.journals.app.converter;

import java.io.Serializable;

import medical.journals.app.entity.BaseEntity;

/**
 * Interface to define the converter operations between entity and dto.
 * 
 * @author Clauber Ferreira
 * 
 * @param <E> Any entity class
 * @param <O> Any Dto class
 * @param <I> Entity identifier
 */
public interface Converter<E extends BaseEntity<I>, O extends Serializable, I extends Serializable> extends
        Serializable {

    /**
     * Convert a dto object to an entity object
     * 
     * @param dto
     * @param entity
     * @return E
     */
    E convertDtoToEntity(O dto, E entity);

    /**
     * Convert and entity to a dto object
     * 
     * @param entity
     * @param dto
     * @return O
     */
    O convertEntityToDto(E entity, O dto);
}
