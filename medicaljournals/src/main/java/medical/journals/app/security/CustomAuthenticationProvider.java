package medical.journals.app.security;

import java.util.ArrayList;
import java.util.List;

import medical.journals.app.dto.LoginDto;
import medical.journals.app.services.LoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

/**
 * Class that check the user credentials in the database
 * 
 * @author Clauber Ferreira
 */
@Service
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private LoginService loginService;

    @Override
    public Authentication authenticate(Authentication authentication) {
        LoginDto loginDto =
                loginService.loginUser(authentication.getName(), authentication.getCredentials().toString());

        // Your custom authentication logic here
        if (loginDto != null) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            return new UsernamePasswordAuthenticationToken(loginDto.getMemberDto(), loginDto.getPassword(),
                    authorities);
        }

        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
