package medical.journals.app.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import medical.journals.app.util.Constants;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class for Spring Security.
 * 
 * @author Clauber Ferreira
 */
public class SecurityUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    private SecurityUtils() {
    }

    /**
     * Send an error message enclosed in the ResponseEntity class as a json format to be handled by the
     * client API
     * 
     * @param response
     * @param exception
     * @param status
     * @throws IOException
     */
    public static void sendError(HttpServletResponse response, Exception exception, HttpStatus status)
            throws IOException {
        response.setContentType(Constants.JSON_CONTENT_TYPE_CHARSET_UTF8);
        response.setStatus(status.value());
        PrintWriter writer = response.getWriter();
        writer.write(mapper.writeValueAsString(new ResponseEntity<String>(exception.getMessage(), status)));
        writer.flush();
        writer.close();
    }

    /**
     * Send a message as a json format to be handled by the client API.
     * 
     * @param response
     * @param status
     * @param object
     * @throws IOException
     */
    public static void sendResponse(HttpServletResponse response, HttpStatus status, Object object)
            throws IOException {
        response.setContentType(Constants.JSON_CONTENT_TYPE_CHARSET_UTF8);
        PrintWriter writer = response.getWriter();
        writer.write(mapper.writeValueAsString(object));
        response.setStatus(status.value());
        writer.flush();
        writer.close();
    }

}
