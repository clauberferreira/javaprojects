package medical.journals.app.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * 
 * Authentication failure handler for integration with SPA applications that need to login using Ajax instead
 * of a form post.
 * 
 * Detects if its a ajax login request, and if so sends a customized response in the body, otherwise defaults
 * to the existing behaviour for none-ajax login attempts.
 * 
 * @author Clauber Ferreira
 */
public class AjaxAuthenticationFailureHandler implements AuthenticationFailureHandler {

    /**
     * Default constructor
     * 
     * @param defaultHandler
     */
    public AjaxAuthenticationFailureHandler(AuthenticationSuccessHandler defaultHandler) {
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
        SecurityUtils.sendError(response, exception, HttpStatus.BAD_REQUEST);
    }
}
