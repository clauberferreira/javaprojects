package medical.journals.app.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import medical.journals.app.security.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class responsible to handle the general exception and throws to front-end a friendly message.
 * 
 * @author Clauber Ferreira
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * Handle the general Exception
     * 
     * @param request
     * @param response
     * @param ex
     * @throws IOException
     */
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Exception occured")
    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletRequest request, HttpServletResponse response, Exception ex)
            throws IOException {
        LOGGER.error("Exception Occured:: URL=" + request.getRequestURL());
        SecurityUtils.sendError(response, ex, HttpStatus.INTERNAL_SERVER_ERROR);

    }
}
