package medical.journals.app.exception;

/**
 * Exception that is thrown always when some business issue happen.
 * 
 * @author Clauber Ferreira
 * 
 */
public class BusinessException extends Exception {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with the specified cause.
     * 
     * @param e Exception
     */
    public BusinessException(Exception e) {
        super(e);
    }

    /**
     * Constructs a new exception with the specified message.
     * 
     * @param message String
     */
    public BusinessException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message.
     * 
     * @param message String
     * @param cause Throwable
     */
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
