package medical.journals.app.exception;

/**
 * Runtime exception that is thrown always when some technical issue happen.
 * 
 * @author Clauber Ferreira
 * 
 */
public class TechnicalException extends RuntimeException {

    /**
     * Serial Version UID for this class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new runtime exception with the specified cause.
     * 
     * @param e Exception
     */
    public TechnicalException(Exception e) {
        super(e);
    }

    /**
     * Constructs a new runtime exception with the specified message.
     * 
     * @param message String
     */
    public TechnicalException(String message) {
        super(message);
    }

    /**
     * Constructs a new runtime exception with the specified cause and a detail message.
     * 
     * @param message String
     * @param cause Throwable
     */
    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }
}
