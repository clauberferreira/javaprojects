package medical.journals.app.util;

/**
 * Class to define all the constants values used across the application.
 * 
 * @author Clauber Ferreira
 */
public final class Constants {

    /**
     * encoding utf-8
     */
    public static final String UTF8 = "UTF-8";

    /**
     * SuppressWarnings unchecked.
     */
    public static final String UNCHECKED = "unchecked";

    /**
     * Temporary location where files will be stored
     */
    public static final String LOCATION_UPLOAD = "C:\\temp\\";

    /**
     * Max file size: 5MB. Beyond that size spring will throw exception.
     */
    public static final Long MAX_FILE_SIZE = 5242880L;

    /**
     * Total request size containing Multi part: 20mb.
     */
    public static final Long MAX_REQUEST_SIZE = 20971520L;

    /**
     * Size threshold after which files will be written to disk
     */
    public static final Integer FILE_SIZE_THRESHOLD = 0;

    /**
     * PDF file format
     */
    public static final String PDF_FILE = ".pdf";

    /**
     * PDF content type
     */
    public static final String PDF_CONTENT_TYPE = "application/pdf";

    /**
     * JSON content type
     */
    public static final String JSON_CONTENT_TYPE_CHARSET_UTF8 = "application/json;charset=UTF-8";

    /**
     * One megabyte represented in bytes.
     */
    public static final Long ONE_MB_FILE_SIZE = 1048576L;

    private Constants() {

    }
}
