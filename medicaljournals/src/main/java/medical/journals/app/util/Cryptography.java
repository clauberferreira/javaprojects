package medical.journals.app.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import medical.journals.app.exception.TechnicalException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to cryptography any string value using the algorithm SHA-256
 * 
 * @author Clauber Ferreira
 */
public class Cryptography {

    private static final Logger LOGGER = LoggerFactory.getLogger(Cryptography.class);

    private static final String SHA256_ALGORITHM = "SHA-256";

    private Cryptography() {
    }

    /**
     * Cryptography the value
     * 
     * @param value
     * 
     * @return String
     */
    public static String cryptography(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(SHA256_ALGORITHM);
            return Base64.encodeBase64String(messageDigest.digest(value.getBytes(Constants.UTF8)));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new TechnicalException(e.getMessage(), e);
        }
    }
}
