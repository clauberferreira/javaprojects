package medical.journals.app.controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import medical.journals.app.exception.BusinessException;
import medical.journals.app.security.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Class that define default controller operations.
 * 
 * @author Clauber Ferreira
 */
@Controller
public class BaseController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    /**
     * Handle the BusinessException
     * 
     * @param request
     * @param response
     * @param ex
     * @throws IOException
     */
    @ExceptionHandler(BusinessException.class)
    public void handleException(HttpServletRequest request, HttpServletResponse response, Exception ex)
            throws IOException {
        LOGGER.error("Exception Occured:: URL=" + request.getRequestURL());
        SecurityUtils.sendError(response, ex, HttpStatus.BAD_REQUEST);
    }
}
