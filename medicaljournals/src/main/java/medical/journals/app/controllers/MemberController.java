package medical.journals.app.controllers;

import medical.journals.app.dto.MemberDto;
import medical.journals.app.exception.BusinessException;
import medical.journals.app.services.MemberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class that define operations to operate with member entity that can be a publisher or a subscriber.
 * 
 * @author Clauber Ferreira
 */
@RestController
@RequestMapping("/member")
public class MemberController extends BaseController {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private MemberService memberService;

    /**
     * create new member
     * 
     * @param memberDto
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createNewMember(@RequestBody MemberDto memberDto) throws BusinessException {
        memberService.createNewMember(memberDto);
    }

    /**
     * update an existing member
     * 
     * @param memberDto
     * @return Updated memberDto
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public MemberDto updateMember(@RequestBody MemberDto memberDto) throws BusinessException {
        return memberService.updateMember(memberDto);
    }

    /**
     * find a registered member
     * 
     * @param id
     * @return Member
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    @ResponseBody
    public MemberDto findMember(@PathVariable("id") Long id) throws BusinessException {
        return memberService.findMember(id);
    }

}
