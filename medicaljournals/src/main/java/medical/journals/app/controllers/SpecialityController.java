package medical.journals.app.controllers;

import java.util.List;

import medical.journals.app.dto.SpecialityDto;
import medical.journals.app.exception.BusinessException;
import medical.journals.app.services.SpecialityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class that define operations to operate with speciality entity.
 * 
 * @author Clauber Ferreira
 */
@RestController
@RequestMapping("/speciality")
public class SpecialityController extends BaseController {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private SpecialityService specialityService;

    /**
     * find all registered specialities
     * 
     * @return List<SpecialityDto>
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    @ResponseBody
    public List<SpecialityDto> findAllSpecialities() throws BusinessException {
        return specialityService.findAllSpecialities();
    }

}
