package medical.journals.app.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import medical.journals.app.dto.PublicationDto;
import medical.journals.app.exception.BusinessException;
import medical.journals.app.services.PublicationService;
import medical.journals.app.util.Constants;

import org.hsqldb.lib.InOutUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class that define operations to operate with Publication entity and receive the file upload.
 * 
 * @author Clauber Ferreira
 */
@RestController
@RequestMapping("/publication")
public class PublicationController extends BaseController {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private PublicationService publicationService;

    /**
     * create new publication
     * 
     * @param publicationDto
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createNewPublication(@RequestBody PublicationDto publicationDto) throws BusinessException {
        publicationService.createNewPublication(publicationDto);
    }

    /**
     * update an existing publication
     * 
     * @param publicationDto
     * @return Updated publicationDto
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public PublicationDto updatePublication(@RequestBody PublicationDto publicationDto)
            throws BusinessException {
        return publicationService.updatePublication(publicationDto);
    }

    /**
     * find a registered publication
     * 
     * @param id
     * @return Publication
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    @ResponseBody
    public PublicationDto findPublication(@PathVariable("id") Long id) throws BusinessException {
        return publicationService.findPublication(id);
    }

    /**
     * Search for publications that match with title, content, or speciality
     * 
     * @param search the searched word
     * @return Some matching records
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/search/{search}", method = RequestMethod.GET)
    @ResponseBody
    public List<PublicationDto> searchPublications(@PathVariable("search") String search)
            throws BusinessException {
        return publicationService.searchPublications(search);
    }

    /**
     * Upload the file to the configured directory.
     * 
     * @param file MultipartFile
     * @throws BusinessException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public void uploadFile(@RequestParam(value = "file", required = true) MultipartFile file)
            throws BusinessException {
        publicationService.uploadFile(file);
    }

    /**
     * Download the file from the configured directory.
     * 
     * @param fileName String
     * @param response
     * @throws IOException
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/download/{fileName}", method = RequestMethod.GET)
    public void
            downloadFile(@PathVariable(value = "fileName") String fileName, HttpServletResponse response)
                    throws IOException {
        File file = new File(Constants.LOCATION_UPLOAD.concat(fileName).concat(Constants.PDF_FILE));

        response.setContentType(Constants.PDF_CONTENT_TYPE);

        InputStream in = new FileInputStream(file.getAbsolutePath());
        InOutUtil.copy(in, response.getOutputStream());
        in.close();
        response.getOutputStream().close();
    }
}
