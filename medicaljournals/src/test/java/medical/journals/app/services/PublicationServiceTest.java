package medical.journals.app.services;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import medical.journals.app.dto.PublicationDto;
import medical.journals.app.exception.BusinessException;
import medical.journals.app.exception.TechnicalException;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class to execute unit tests to the PublicationService implementation.
 * 
 * @author Clauber Ferreira
 */
@SuppressWarnings("javadoc")
public class PublicationServiceTest extends BaseTest {

    @Autowired
    @Qualifier(ConstantsService.PUBLICATION_SERVICE_NAME)
    private PublicationService publicationService;

    @Autowired
    @Qualifier(ConstantsService.MEMBER_SERVICE_NAME)
    private MemberService memberService;

    @Autowired
    @Qualifier(ConstantsService.SPECIALITY_SERVICE_NAME)
    private SpecialityService specialityService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void createNewPublicationFailureMissingFillFields() throws BusinessException {
        exception.expect(ConstraintViolationException.class);
        publicationService.createNewPublication(new PublicationDto());
    }

    @Test
    public void createNewPublicationSuccessfully() throws BusinessException {
        PublicationDto publicationDto = new PublicationDto();
        publicationDto.setContent("test");
        publicationDto.setEdition(new Date());
        publicationDto.setFile("test");
        publicationDto.setPublisher(memberService.findMember(1L));
        publicationDto.setSpeciality(specialityService.findAllSpecialities().get(0));
        publicationDto.setTitle("test");

        publicationService.createNewPublication(publicationDto);
    }

    @Test
    public void updatePublicationSuccessfully() throws BusinessException {
        PublicationDto publicationDto = publicationService.findPublication(1L);
        publicationDto.setTitle("test");
        publicationDto.setContent("test");
        publicationService.updatePublication(publicationDto);
    }

    @Test
    public void findPublicationFailureNotExit() throws BusinessException {
        Assert.assertThat(publicationService.findPublication(10L), Matchers.nullValue());
    }

    @Test
    public void findPublicationSuccessfully() throws BusinessException {
        Assert.assertThat(publicationService.findPublication(1L), Matchers.notNullValue());
    }

    @Test
    public void uploadFileFaiulureNotPermittedExtension() throws TechnicalException, BusinessException {
        exception.expect(BusinessException.class);
        exception.expectMessage("This file isn't a PDF format.");

        MultipartFile file = new MockMultipartFile("test.png", "test.png", null, new byte[10]);
        publicationService.uploadFile(file);
    }

    @Test
    public void uploadFileFaiulureSizeTooBig() throws TechnicalException, BusinessException {
        exception.expect(BusinessException.class);

        MultipartFile file = new MockMultipartFile("test.pdf", "test.pdf", null, new byte[10000000]);
        publicationService.uploadFile(file);
    }

    @Test
    public void uploadFileSucessfuly() throws TechnicalException, BusinessException {
        MultipartFile file = new MockMultipartFile("test.pdf", "test.pdf", null, new byte[100]);
        publicationService.uploadFile(file);
    }

    @Test
    public void searchPublicationSuccessfully() throws BusinessException {
        Assert.assertThat(publicationService.searchPublications("a"), Matchers.not(Matchers.empty()));
    }
}
