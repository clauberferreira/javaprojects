package medical.journals.app.services;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import medical.journals.app.entity.Speciality;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Class to execute unit tests to the BaseService implementation for Speciality entity.
 * 
 * @author Clauber Ferreira
 */
@SuppressWarnings("javadoc")
public class BaseServiceTest extends BaseTest {

    @Autowired
    @Qualifier(ConstantsService.BASE_SERVICE_NAME)
    private BaseService<Speciality, Long> specialityService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void createNewEntitySuccess() {
        Speciality psychology = new Speciality();
        psychology.setName("psychology");
        specialityService.persist(psychology);

        Assert.assertThat(specialityService.findAll(Speciality.class), Matchers.hasSize(11));
    }

    @Test
    public void createNewEntityFailureExceedingMaxLenthName() {
        exception.expect(ConstraintViolationException.class);
        Speciality psychology = new Speciality();
        psychology.setName("psychology psychology psychology");
        specialityService.persist(psychology);

        chechHowManyRecordsThereAreInDatabase(10);
    }

    @Test
    public void updateEntitySuccess() {
        Speciality psychiatrist = specialityService.find(Speciality.class, 1L);
        psychiatrist.setName("psychiatrist");
        specialityService.merge(psychiatrist);

        Assert.assertThat(specialityService.find(Speciality.class, 1l).getName(),
                Matchers.equalTo("psychiatrist"));
    }

    @Test
    public void updateEntityFailureExceedingMaxLenthName() {
        exception.expect(ConstraintViolationException.class);
        Speciality psychiatrist = specialityService.find(Speciality.class, 1L);
        psychiatrist.setName("psychiatrist psychiatrist psychiatrist");
        specialityService.merge(psychiatrist);

        chechHowManyRecordsThereAreInDatabase(10);
    }

    @Test
    public void removeEntitySuccess() {
        Speciality speciality = specialityService.find(Speciality.class, 2l);
        specialityService.remove(speciality);

        chechHowManyRecordsThereAreInDatabase(9);
    }

    @Test
    public void removeEntityFailureConstraintViolation() {
        exception.expect(PersistenceException.class);
        Speciality speciality = specialityService.find(Speciality.class, 1l);
        specialityService.remove(speciality);

        chechHowManyRecordsThereAreInDatabase(10);
    }

    private void chechHowManyRecordsThereAreInDatabase(int amout) {
        Assert.assertThat(specialityService.findAll(Speciality.class), Matchers.hasSize(amout));
    }

}
