package medical.journals.app.services;

import medical.journals.config.root.RootContextConfig;
import medical.journals.config.root.TestConfiguration;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class to configure the resources test to be used by another test class.
 * 
 * @author Clauber Ferreira
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = { TestConfiguration.class, RootContextConfig.class })
@Transactional
public abstract class BaseTest {

}
