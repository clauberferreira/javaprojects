package medical.journals.app.services;

import medical.journals.app.dto.MemberDto;
import medical.journals.app.exception.BusinessException;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Class to execute unit tests to the MemberService implementation.
 * 
 * @author Clauber Ferreira
 */
@SuppressWarnings("javadoc")
public class MemberServiceTest extends BaseTest {

    @Autowired
    @Qualifier(ConstantsService.MEMBER_SERVICE_NAME)
    private MemberService memberService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void findMemberByEmailFailureIncorrectEmail() {
        Assert.assertThat(memberService.findMemberByEmail("aaa"), Matchers.nullValue());
    }

    @Test
    public void findMemberByEmailSuccessfully() {
        Assert.assertThat(memberService.findMemberByEmail("clauber.ferreira@gmail.com"),
                Matchers.notNullValue());
    }

    @Test
    public void createNewMemberFailureIncorrectConfirmationPassword() throws BusinessException {
        exception.expect(BusinessException.class);
        exception.expectMessage("The password confirmation is invalid.");

        MemberDto memberDto = new MemberDto();
        memberDto.setEmail("test@gmail.com");
        memberDto.setMemberType("PUBLISHER");
        memberDto.setName("test");
        memberDto.setPassword("123");
        memberDto.setConfirmPassword("12");

        memberService.createNewMember(memberDto);
        Assert.assertThat(memberDto.getId(), Matchers.notNullValue());
    }

    @Test
    public void createNewMemberSuccessfully() throws BusinessException {
        MemberDto memberDto = new MemberDto();
        memberDto.setEmail("test@gmail.com");
        memberDto.setMemberType("PUBLISHER");
        memberDto.setName("test");
        memberDto.setPassword("123");
        memberDto.setConfirmPassword("123");

        memberService.createNewMember(memberDto);
    }

    @Test
    public void updateMemberSuccessfully() throws BusinessException {
        MemberDto memberDto = new MemberDto();
        memberDto.setId(1L);
        memberDto.setEmail("clauber.ferreira@gmail.com");
        memberDto.setMemberType("PUBLISHER");
        memberDto.setName("test");
        memberDto.setPassword("test");
        memberDto.setConfirmPassword("test");

        memberService.updateMember(memberDto);
    }

    @Test
    public void findMemberFailureNotExit() throws BusinessException {
        Assert.assertThat(memberService.findMember(10L), Matchers.nullValue());
    }

    @Test
    public void findMemberSuccessfully() throws BusinessException {
        Assert.assertThat(memberService.findMember(1L), Matchers.notNullValue());
    }
}
