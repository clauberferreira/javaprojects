package medical.journals.app.services;

import medical.journals.app.dto.LoginDto;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Class to execute unit tests to the LoginService implementation.
 * 
 * @author Clauber Ferreira
 */
@SuppressWarnings("javadoc")
public class LoginServiceTest extends BaseTest {

    @Autowired
    @Qualifier(ConstantsService.LOGIN_SERVICE_NAME)
    private LoginService loginService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void loginFailureIncorrectUsername() {
        exception.expect(UsernameNotFoundException.class);
        LoginDto loginDto = loginService.loginUser("test", "test");
        Assert.assertThat(loginDto, Matchers.nullValue());
    }

    @Test
    public void loginFailureIncorrectPassword() {
        exception.expect(BadCredentialsException.class);
        LoginDto loginDto = loginService.loginUser("clauber.ferreira@gmail.com", "aaa");
        Assert.assertThat(loginDto, Matchers.nullValue());
    }

    @Test
    public void loginSuccessfully() {
        LoginDto loginDto = loginService.loginUser("clauber.ferreira@gmail.com", "test");
        Assert.assertThat(loginDto, Matchers.notNullValue());
    }
}
